const users = [
    {
       uid: 001,
       email: 'john@dev.com',
       personalInfo: {
          name: 'John',
          address: {
             line1: 'westwish st',
             line2: 'washmasher',
             city: 'wallas',
             state: 'WX'
          }
       }
    },
    {
       uid: 063,
       email: 'a.abken@larobe.edu.au',
       personalInfo: {
          name: 'amin',
          address: {
             line1: 'Heidelberg',
             line2: '',
             city: 'Melbourne',
             state: 'VIC'
          }
       }
    },
    {
       uid: 045,
       email: 'Linda.Paterson@gmail.com',
       personalInfo: {
          name: 'Linda',
          address: {
             line1: 'Cherry st',
             line2: 'Kangaroo Point',
             city: 'Brisbane',
             state: 'QLD'
          }
       }
    }
 ]

 function returnUsers(users) {    
    usersObj = new Array();
    for(var i=0; i<users.length;i++){
        each_user = {}
        var name = users[i].personalInfo.name;
        var email = users[i].email;
        var theState = users[i].personalInfo.address.state;
        each_user = {name:name, email:email, state:theState};
        usersObj.push(each_user);
    }
    return usersObj;
}
//var returnUsers = returnUsers();

//document.getElementById('show_users').addEventListener('click',show_users);

//Task6
function show_users(){
    var returnList = returnUsers(users);
    html ="<table>";
    for (var i = 0; i < usersObj.length; i++) { // create rows 
        
      html += "<tr>"
      
        html += "<td>" + usersObj[i].name + "</td>";
        html += "<td>" + usersObj[i].email + "</td>";
        html += "<td>" + usersObj[i].state + "</td>";
        
      html += "</tr>"
    }
    html += "</table>";
    document.getElementById('tableId').innerHTML = html;
}


//Task 7
function sortUser(){
    var getUser = returnUsers(users);
    getUser.sort(function(a,b){
            var x = a.name.toLowerCase();
            var y = b.name.toLowerCase();
            if (x > y){return 1;}
            if (x < y){return -1;}
            return 0;
            
        });
    return getUser;
}
sortUser();

function displaySortedUsers(){
    var getList = sortUser(users);
    html2 = "<table>";
    for (var i = 0; i < getList.length; i++) { // create rows 
        
        html2 += "<tr>"
        
          html2 += "<td>" + getList[i].name + "</td>";
          html2 += "<td>" + getList[i].email + "</td>";
          html2 += "<td>" + getList[i].state + "</td>";
          
        html2 += "</tr>"
      }
      html2 += "</table>";
      document.getElementById('tableId').innerHTML = html2;


}





